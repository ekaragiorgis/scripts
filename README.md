A collection of convenient scripts that should speed up local development.

## Set up
Since these are all bash scripts we can add their root folder in the user path and execute them from anywhere on the machine.

To do that:

1. Clone the repo to a whichever folder you want.
2. Add the following entry to your ~/.bash_profile: \
    `export PATH="{scripts_folder}:$PATH"`

Alternatively, you can copy the scripts to /usr/local/bin which is already in the PATH

## Collection

### docker-reset.sh
Stops any running docker containers and removes them.
Removes all existing docker volumes.
Optionally remove all docker images by passing '-a'

Usage: \
&nbsp;&nbsp;&nbsp;&nbsp;docker-reset.sh
	
&nbsp;&nbsp;&nbsp;&nbsp;flags: \
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-a: Removes all docker images as well \
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-h: Show help text

### update-migration-log.sh


Checks if the latest migrations have been added to the migration log
If not, it will add them with the correct spacing and names

NOTE: Currently only able to update one set of migrations add a time.
It assumes that the new migrations are added as a set of four files and are
named *_source_hasura.go, *_db.go, *_target_hasura.go, *_target_hasura.json.

Run from the ../database/migrations/migration folder

Usage: \
&nbsp;&nbsp;&nbsp;&nbsp;update-migration-log.sh

&nbsp;&nbsp;&nbsp;&nbsp;flags: \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-h: Show help text

### curl-hasura-metadata.sh


Pulls the latest hasura metada using curl.
The name of the file will be that of the latest target_hasura.go but the extension will be .json.
Also copies the latest metadata pulled to latest.json.

Run from the ../database/migrations/migration folder

Usage:
&nbsp;&nbsp;&nbsp;&nbsp;curl-hasura-metadata.sh

&nbsp;&nbsp;&nbsp;&nbsp;flags:
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-h: Show help text
