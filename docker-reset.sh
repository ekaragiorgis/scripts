#!/usr/bin/env bash

help="
Stops any running docker containers and removes them.
Removes all existing docker volumes.
Optionally remove all docker images by passing '-a'

Usage:
    docker-reset.sh

    flags:
        -a: Removes all docker images as well
        -h: Show help text
"

if [ "$1" == "-h" ]; then
    echo "$help"
    exit 0
fi

docker_containers=$(docker ps -aq | head -1)

if [ $docker_containers ]; then
    echo "running containers found, stopping and removing"
    docker stop $(docker ps -aq)
    docker rm -vf $(docker ps -aq)
else
    echo "No containers found"
fi

docker_volumes=$(docker volume ls -q | head -1)

if [ $docker_volumes ]; then
    echo "volumes found, removing"
    docker volume rm $(docker volume ls -q)
else
    echo "No volumes found"
fi

if [ "$1" == "-a" ]; then
    docker_images=$(docker images -q | head -1)

    if [ $docker_images ]; then
        echo "Removing images..."
        docker rmi $(docker images -q)
    else
        echo "No Images found"
    fi
fi
