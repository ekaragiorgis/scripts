#!/usr/bin/env bash

help="
Checks if the latest migrations have been added to the migration log
If not, it will add them with the correct spacing and names

NOTE: Currently only able to update one set of migrations add a time.
It assumes that the new migrations are added as a set of four files and are
named *_source_hasura.go, *_db.go, *_target_hasura.go, *_target_hasura.json.

Run from the ../database/migrations/migration folder

Usage:
    update-migration-log.sh

    flags:
        -h: Show help text
"

if [ "$1" == "-h" ]; then
    echo "$help"
    exit 0
fi

# check current directory is as expected by checking for log location

# SETUP
MIGRATION_LOG="../MIGRATION_LOG.txt"
SQL_SEQ_FILE="SQL SEQUENCE FILE."
HASURA_SEQ_FILE="HASURA SEQUENCE FILE."

check_directory=$(ls $MIGRATION_LOG 2> /dev/null)
exit_code=$?

if [ $exit_code -ne 0 ]; then
    echo "Unable to update. Must run script from the ../database/migrations/migration folder"
    exit 1
fi

create_entry () {
    index=$1
    sql_file=$2
    hasura_file=$3

    whitespace_1=$(( $SQL_SEQ_INDEX - ${#index}))
    whitespace_2=$(( $HASURA_SEQ_INDEX - ${#sql_file} - $SQL_SEQ_INDEX ))

    w_1=""
    w_2=""

    w_1=$(printf "%*s%s" $whitespace_1 '' "$w_1" )
    if [ ${#hasura_file} != 0 ]; then
        w_2=$(printf "%*s%s" $whitespace_2 '' "$w_2" )
    fi

    echo "$index$w_1$sql_file$w_2$hasura_file" >> $MIGRATION_LOG
}

MIGRATION_LOG_HEADER=$(head -1 $MIGRATION_LOG)

temp=${MIGRATION_LOG_HEADER#*$SQL_SEQ_FILE}

SQL_SEQ_INDEX=$(( ${#MIGRATION_LOG_HEADER} - ${#temp} - ${#SQL_SEQ_FILE} ))

temp=${MIGRATION_LOG_HEADER#*$HASURA_SEQ_FILE}
HASURA_SEQ_INDEX=$(( ${#MIGRATION_LOG_HEADER} - ${#temp} - ${#HASURA_SEQ_FILE} ))

# check if latest migrations are in the log

# source hasura
source_hasura=$(ls -r *_source_hasura.go | head -1 | cut -d "." -f 1)
source_exists=$(grep $source_hasura $MIGRATION_LOG)

last_index=$(tail -1 $MIGRATION_LOG | awk '{print $1}' | cut -d "." -f 1)
next_index=$((last_index+1))

# if source exists is empty, we need to add it
# using the previous target hasura in the MIGRATION_LOG
# we also assume that if source does not exist the rest of the migration files
# do not exist as well
if (( ${#source_exists}>0 )); then
    echo "MIGRATION_LOG is already up to date."
    exit 0
else
    previous_target_hasura=$(tail -1 $MIGRATION_LOG | awk '{print $NF}')
    create_entry "$next_index." $source_hasura $previous_target_hasura
fi

# db
db=$(ls -r *_db.go | head -1 | cut -d "." -f 1)

next_index=$((next_index+1))
create_entry "$next_index." $db ""

# target hasura
target_hasura=$(ls -r *target_hasura.go | head -1 | cut -d "." -f 1)
next_index=$((next_index+1))

final_target_hasura=$(ls -r *target_hasura.json | head -1 | cut -d "." -f 1)
create_entry "$next_index." $target_hasura $final_target_hasura

echo "MIGRATION_LOG updated succesfully"
