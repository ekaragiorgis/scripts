#!/usr/bin/env bash

help="
Pulls the latest hasura metada using curl.
The name of the file will be that of the latest target_hasura.go but the 
extension will be .json.
Also copies the latest metadata pulled to latest.json

Run from the ../database/migrations/migration folder

Usage:
    curl-hasura-metadata.sh

    flags:
        -h: Show help text
"

if [ "$1" == "-h" ]; then
    echo "$help"
    exit 0
fi

check_directory=$(ls $MIGRATION_LOG 2> /dev/null)
exit_code=$?

if [ $exit_code -ne 0 ]; then
    echo "Unable to update. Must run script from the ../database/migrations/migration folder"
    exit 1
fi

filename=$(ls -r *_target_hasura.go | head -1 | cut -d "." -f 1)

curl -H 'X-Hasura-Admin-Secret:secret' -d '{"type":"export_metadata","version":1,"args":{}}' http://localhost:8081/v1/metadata -o $filename.json

cp $filename.json latest.json
